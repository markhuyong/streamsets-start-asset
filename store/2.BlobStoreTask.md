# BlobStoreTask

### 个人理解

理论上BlobStore就是存放Pipeline信息的持久数据，通过类图接口的寻找，相关类图如下

![image-20200319173206198](./image/image-20200319173206198.png)

## 抽象层

![image-20200319173535613](./image/image-20200319173535613.png)

BlobStoreTask在抽象层混入了BlobStore存储和Task两个接口的功能：

- BlobStore中阐述了对Pipeline存储的数据应该是黑盒的，所以名称定义了Blob作为前缀。（但从`store`和`retrieve`方法的定义上来看，都以String类型作为存储对象，所以个人以为更应该叫做Clob :-）

  `General store for various data used by SDC and its various components. 
  This store does not interpret the data in any way and deals with them as a black box. 
  Storing and retrieving them by various components. Agreeing on proper data format and schema is up to the writer and caller. 
  The objects within the store are separated into namespaces - each namespace is independent of each other and can contain objects with the same id. 
  All objects are versioned and the store can contain the same object with multiple versions. 
  The store imposes structural requirements for namespace, object id and version - all those must comport to exposed format. 
  However the semantics of the values is not imposed and is left to the caller. Content itself is not interpreted in any way.`

- Task是SDC的一个上层抽象，只要是IO操作，耗费时间较长的都会实现，并多为继承其模版类AbstractTask为主。
  
  - Task内部定义了一个enum Status，对Task的状态初始定义，并在AbstractTask中定义了状态之间的转换规则，同时也在各阶段的模版方法中进行了状态转换的校验操作。
  - BlobStoreTask混入了Task的功能，也是为了把存储操作进行分层分级，并且可以进行线程拆分。
  
- BlobStore中还定义了一个子接口VersionedContent，用于定义在存储中获取对应的内容Content是要绑定一个long类型的版本version。

  - 从获取存储内容的主要函数定义上，也可以看出store的内容是包括namespace、id、version三个层级进行存储和管理的。

    ```java
    public String retrieve(String namespace, String id, long version) throws StageException;
    ```

  - 猜测接口中定义的StageUpgrader及其后续实现，也和检测这个版本号有关。

## 持久层

这里来到SDC的Container核心模块（个人感觉叫coreContainer更合适）。其内部对于BlobStoreTask的实现类为`BlobStoreTaskImpl`，其内部对于存储进行了三层结构的分解。

![image-20200319182238079](./image/image-20200319182238079.png)

从主要的成员变量来看，该类内容主要包括：

- RuntimeInfo，用于保存运行时元数据信息，例如某Pipeline的元数据存储到哪个目录前缀中。
- BlobStoreMetadata，持久层结构引用入口。

下面主要来看看类`BlobStoreMetadata`及其相关类的结构：

![image-20200319182743777](./image/image-20200319182743777.png)

以上三个类中，都分别对其成员变量提供了Json序列化的方法，默认的该数据被持久化到了`${SDC_HOME}/data/blobstore`目录中。

## 运行时

从BootstrapMain类中根据预设规则进行配置目录的扫描，生成类的加载顺序，反射调用DataCollectorMain类的两个方法：

- ```java
  public class DataCollectorMain extends Main {
      public static void setContext(
            ClassLoader apiCL,
            ClassLoader containerCL,
            List<? extends ClassLoader> moduleCLs,
            Instrumentation instrumentation
        ) {
          // 在这里就将生成好的类加载顺序传递，再次进行选择和实例化RuntimeInfo
          RuntimeModule.setStageLibraryClassLoaders(moduleCLs);
        }
}
  
  
  @Module(library = true, injects = {
      BuildInfo.class,
      RuntimeInfo.class,
      Configuration.class,
      EventListenerManager.class,
      UserGroupManager.class
  }, includes = MetricsModule.class)
  public class RuntimeModule {
      ...
      // 静态方法，将classLoaders存放于该类
      public static synchronized void setStageLibraryClassLoaders(List<? extends ClassLoader> classLoaders) {
      	stageLibraryClassLoaders = ImmutableList.copyOf(classLoaders);
      }
  	...
      // 通过这里，终于发现，初始化的RuntimeInfo是StandaloneRuntimeInfo这个可实例化的类。
      @Provides @Singleton
    	public RuntimeInfo provideRuntimeInfo(MetricRegistry metrics) {
      	RuntimeInfo info = new StandaloneRuntimeInfo(productName, propertyPrefix, metrics, stageLibraryClassLoaders);
          info.init();
          return info;
      }
      ...
  }
  
  ```
  
- ```java
  public static void main(String[] args) throws Exception {
    int exitStatus = 0;
    if(!WebServerAgentCondition.canContinue()) {
      exitStatus = new DataCollectorMain(
          MainStandalonePipelineManagerModule.class,
          WebServerAgentCondition::getReceivedCredentials
      ).doMain();
    }
    if (exitStatus == 0) {
      exitStatus = new DataCollectorMain().doMain();
    }
    System.exit(exitStatus);
  }
  ```

### 推测

`RuntimeModule`类顶部的@Module注解

- injects属性包含的内容，应该是该类被实例化出来，可以被其他类进行注入。
- library = true 一般就这么配置，因为有可能会多写一些Provider的方法，但却没有被其他模块inject注入，会导致成为死代码。如果后期优化时，可以考虑改成false，来优化内存占用。
- includes = MetricsModule.class，这个注解，应该是调用指定类中包含`@Provides`注解的方法。So，看来Dagger也可以通过maven的dependency和跨包@Module(includes)方式来集成调用。

`MainStandalonePipelineManagerModule`类顶部的@Module注解还包括一个complete属性。还有相关原作者写了大篇的备注，看来也是为了学习和交流。

```java
package com.streamsets.datacollector.main;

import com.streamsets.datacollector.event.handler.dagger.EventHandlerModule;
import com.streamsets.datacollector.execution.Manager;
import com.streamsets.datacollector.execution.manager.standalone.StandaloneAndClusterPipelineManager;
import com.streamsets.datacollector.execution.manager.standalone.dagger.StandalonePipelineManagerModule;
import com.streamsets.datacollector.http.WebServerModule;
import com.streamsets.datacollector.store.PipelineStoreTask;
import com.streamsets.datacollector.task.Task;
import com.streamsets.datacollector.task.TaskWrapper;
import com.streamsets.datacollector.util.Configuration;

import dagger.Module;
import dagger.ObjectGraph;
import dagger.Provides;

import javax.inject.Singleton;

/**
 * Provides singleton instances of RuntimeInfo, PipelineStoreTask and PipelineTask
 */
@Module(
  injects = {TaskWrapper.class, RuntimeInfo.class, Configuration.class, PipelineStoreTask.class, LogConfigurator.class, BuildInfo.class},
  library = true,
  complete = false /* Note that all the bindings are not supplied so this must be false */
)
public class MainStandalonePipelineManagerModule { //Need better name

  private final ObjectGraph objectGraph;

  public MainStandalonePipelineManagerModule() {

    ObjectGraph objectGraph = ObjectGraph.create(StandalonePipelineManagerModule.class);
    Manager m = new StandaloneAndClusterPipelineManager(objectGraph);

    // What did we just do here?
    //1. Injected fields in StandalonePipelineManager using the StandalonePipelineManagerModule module.
    //    在StandalonePipelineManager中使用StandalonePipelineManagerModule模块注入字段。
    //2. Cached Object graph in StandalonePipelineManager. 在StandalonePipelineManager缓存的对象图。

    // Ok. Why do we need to cache the object graph? Why cant we use the old fashioned constructor injection?
    // 好的。为什么需要缓存对象图?为什么我们不能使用老式的构造函数注入呢?
    // Consider this case: ProductionPipeline is constructed using dagger and it depends on
    // AlertManager [one per runner/pipeline] and it also depends on pipeline store [which is a sdc/manager level
    // singleton].
    // 考虑一下这种情况:ProductionPipeline是使用dagger构建的，
    // 它依赖于AlertManager[每个运行器/管道一个]，
    // 它还依赖于管道存储[这是sdc/manager级别的单例模式]。
    // This means when multiple pipelines are created, each owns an instance of an alert manager but all refer to the
    // same instance of pipeline store.
    // 这意味着在创建多个管道时，每个管道都拥有一个警报管理器实例，但都引用管道存储的相同实例。


    //Once the manager is created, augment the object graph with web server and pipeline task modules.
    // 一旦创建了管理器，就可以使用web服务器和管道任务模块来扩展对象图。
    //This ensures that
    // 这将确保
    //1. PipelineTask references the above pipeline manager
    // - 1. PipelineTask引用上面的管道管理器
    //2. Both PipelineTask and PipelineManager refer to the same instance of RuntimeInfo, PipelineStore which is
    // "  a sdc level singleton.
    // - 2. PipelineTask和PipelineManager都指向同一个实例RuntimeInfo, PipelineStore是sdc级单例。
      this.objectGraph = objectGraph.plus(new WebServerModule(m), EventHandlerModule.class, PipelineTaskModule.class);
  }

  @Provides @Singleton
  public Task providePipelineTask(PipelineTask agent) {
    return agent;
  }

  @Provides @Singleton
  public PipelineTask providePipelineTask() {
    return objectGraph.get(PipelineTask.class);
  }

  @Provides @Singleton
  public RuntimeInfo provideRuntimeInfo() {
    return objectGraph.get(RuntimeInfo.class);
  }

  @Provides @Singleton
  public PipelineStoreTask providePipelineStoreTask() {
    return objectGraph.get(PipelineStoreTask.class);
  }

  @Provides @Singleton
  public BuildInfo provideBuildInfo() {
    return objectGraph.get(BuildInfo.class);
  }

  @Provides @Singleton
  public Configuration provideConfiguration() {
    return objectGraph.get(Configuration.class);
  }
}

```

### 回归RuntimeInfo正题

![RuntimeInfo](./image/RuntimeInfo.png)

从属性上看，RuntimeInfo存储了大量的元数据信息。下面的实现类，StandaloneRuntimeInfo和SlaveRuntimeInfo主要是SDC可以单机单实例部署和分布式部署。



