# 9.ECS编译部署

因streamsets-datacollector-edge工程的gradle.properties配置文件中，未写全所有的传递依赖项，且golang.org相关库的过程版本，也未全部映射到Github站点。所以我们在本地或国内的服务器上可能无法编译成功edge工程。



go工程从1.11之后推荐采用go-moudle方式构建工程，但streamsets-datacollector-edge是一直采用gogradle方式进行构建的。



未保证能够编译成功，需要科学上网环境。所以可以采用一台香港的ECS服务器。

![image-20200403234730125](./image/image-20200403234730125.png)

配置参数：

地域：香港

架构：x86

分类：共享型

这里选择了：突发性能实例 t6，因为4G内存是必要的，我先前买的500M的，编译不断地因crash导致不断升配。

![image-20200403235122835](./image/image-20200403235122835.png)



## 环境搭建和编译

可参考[8.WSL 部署StreamSets](./8.WSL 部署StreamSets.md)
