

# ElementType

The constants of this enumerated type provide a simple classification of the syntactic locations where annotations may appear in a Java program. These constants are used in java.lang.annotation.Target meta-annotations to specify where it is legal to write annotations of a given type.

这个枚举类型的常量提供了一个简单的语法位置分类，其中注释可能出现在Java程序中。这些常量在java-lang-annotation-Target元注释中用于指定在什么地方可以合法地编写给定类型的注释



The syntactic locations where annotations may appear are split into declaration contexts , where annotations apply to declarations, and type contexts , where annotations apply to types used in declarations and expressions.

注释可能出现的语法位置被分成声明上下文(注释适用于声明)和类型上下文(注释适用于声明和表达式中使用的类型)。



The constants ANNOTATION_TYPE , CONSTRUCTOR , FIELD , LOCAL_VARIABLE , METHOD , PACKAGE , PARAMETER , TYPE , and TYPE_PARAMETER correspond to the declaration contexts in JLS 9.6.4.1.
For example, an annotation whose type is meta-annotated with @Target(ElementType.FIELD) may only be written as a modifier for a field declaration.

常量ANNOTATION_TYPE、构造函数、字段、LOCAL_VARIABLE、方法、包、参数、类型和TYPE_PARAMETER对应于JLS 9.6.4.1中的声明上下文。

例如，一个类型使用@Target(ElementType.FIELD)进行元注释的注释只能作为字段声明的修饰符来编写。



The constant TYPE_USE corresponds to the 15 type contexts in JLS 4.11, as well as to two declaration contexts: type declarations (including annotation type declarations) and type parameter declarations.
For example, an annotation whose type is meta-annotated with @Target(ElementType.TYPE_USE) may be written on the type of a field (or within the type of the field, if it is a nested, parameterized, or array type), and may also appear as a modifier for, say, a class declaration.

常量TYPE_USE对应于JLS 4.11中的15个类型上下文，以及两个声明上下文:类型声明(包括注释类型声明)和类型参数声明。

例如,注释的类型与@Target meta-annotated (ElementType.TYPE_USE)可能是写在一个字段的类型(或在字段的类型,如果是一个嵌套,参数化,或数组类型),也可以表现为一个修饰词,类声明。



The TYPE_USE constant includes type declarations and type parameter declarations as a convenience for designers of type checkers which give semantics to annotation types. For example, if the annotation type NonNull is meta-annotated with @Target(ElementType.TYPE_USE), then @NonNull class C {...} could be treated by a type checker as indicating that all variables of class C are non-null, while still allowing variables of other classes to be non-null or not non-null based on whether @NonNull appears at the variable's declaration.

TYPE_USE常量包括类型声明和类型参数声明，这为类型检查器的设计人员提供了便利，类型检查器为注释类型提供了语义。例如，如果注释类型NonNull使用@Target(ElementType.TYPE_USE)进行元注释，那么@NonNull类C{…}可以被类型检查器视为表明类C的所有变量都是非空的，同时仍然允许其他类的变量根据@NonNull是否出现在变量的声明中而是非空的。



```java
public enum ElementType {
    /** Class, interface (including annotation type), or enum declaration */
    TYPE,

    /** Field declaration (includes enum constants) */
    FIELD,

    /** Method declaration */
    METHOD,

    /** Formal parameter declaration */
    PARAMETER,

    /** Constructor declaration */
    CONSTRUCTOR,

    /** Local variable declaration */
    LOCAL_VARIABLE,

    /** Annotation type declaration */
    ANNOTATION_TYPE,

    /** Package declaration */
    PACKAGE,

    /**
     * Type parameter declaration
     *
     * @since 1.8
     */
    TYPE_PARAMETER,

    /**
     * Use of a type
     *
     * @since 1.8
     */
    TYPE_USE
}

```

